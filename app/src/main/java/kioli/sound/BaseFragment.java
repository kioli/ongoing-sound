package kioli.sound;

import android.app.Fragment;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class BaseFragment extends Fragment implements OnTouchListener{

    private static final String EXTRA_INSTRUMENT_INDEX = "extra instrument index";

    private SoundPool pool;
    private FrameLayout frame;
    private Instrument instrument;
    private int[] sounds;

    private static final int MINIMUM_DISTANCE = 20;
    private float startX;
    private float startY;

    private final Handler handler = new Handler();

    public static BaseFragment newInstance(final int instrumentIndex) {
        final BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_INSTRUMENT_INDEX, instrumentIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instrument, container, false);
        frame = (FrameLayout) view.findViewById(R.id.instrument_frame);
        frame.setOnTouchListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        instrument = Instrument.values()[getArguments().getInt(EXTRA_INSTRUMENT_INDEX, 0)];
        initPool();
    }

    public void changeInstrument(Instrument instrument) {
        this.instrument = instrument;
        initPool();
    }

    private void initPool() {
        pool = new SoundPool(instrument.getNumberNotes(), AudioManager.STREAM_MUSIC, 0);
        sounds = InstrumentHelper.returnSounds(getActivity(), pool, instrument);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                double x = Math.pow(startX - event.getX(), 2);
                double y = Math.pow(startY - event.getY(), 2);
                if (Math.sqrt(x + y) <  MINIMUM_DISTANCE) {
                    return true;
                } else {
                    startX = event.getX();
                    startY = event.getY();
                }
                break;
            case MotionEvent.ACTION_UP:
                startX = 0;
                startY = 0;
                return true;
        }

        new MyThread(event).start();
        return true;
    }

    private class MyThread extends Thread {
        private MotionEvent event;

        public MyThread(MotionEvent event) {
            this.event = event;
        }

        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    int random = (int) ((event.getX() + event.getY()) % 8);
                    pool.play(sounds[random], 1, 1, 0, 0, 1);

                    final TouchView myV = new TouchView(getActivity());
                    myV.setInfo((int) (event.getX()), (int) (event.getY()));
                    myV.expandMe();
                    frame.addView(myV);
                }
            });
        }
    }

}
