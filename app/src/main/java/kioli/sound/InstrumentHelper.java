package kioli.sound;

import android.content.Context;
import android.media.SoundPool;

public class InstrumentHelper {

    private InstrumentHelper() {
    }

    public static int[] returnSounds(final Context context, final SoundPool pool, final Instrument instrument) {
        int[] sounds;
        switch (instrument) {
            case GUITAR:
                sounds = new int[8];
                sounds[1] = pool.load(context, R.raw.guitar_a, 1);
                sounds[4] = pool.load(context, R.raw.guitar_b, 1);
                sounds[6] = pool.load(context, R.raw.guitar_c, 1);
                sounds[2] = pool.load(context, R.raw.guitar_d, 1);
                sounds[7] = pool.load(context, R.raw.guitar_dm, 1);
                sounds[0] = pool.load(context, R.raw.guitar_e1, 1);
                sounds[5] = pool.load(context, R.raw.guitar_e6, 1);
                sounds[3] = pool.load(context, R.raw.guitar_g, 1);
                break;
            case UKULELE:
                sounds = new int[13];
                sounds[0] = pool.load(context, R.raw.uke_a, 1);
                sounds[1] = pool.load(context, R.raw.uke_a_sharp, 1);
                sounds[2] = pool.load(context, R.raw.uke_b, 1);
                sounds[3] = pool.load(context, R.raw.uke_c1, 1);
                sounds[4] = pool.load(context, R.raw.uke_c2, 1);
                sounds[5] = pool.load(context, R.raw.uke_c_sharp, 1);
                sounds[6] = pool.load(context, R.raw.uke_d, 1);
                sounds[7] = pool.load(context, R.raw.uke_d_sharp, 1);
                sounds[8] = pool.load(context, R.raw.uke_e, 1);
                sounds[9] = pool.load(context, R.raw.uke_f, 1);
                sounds[10] = pool.load(context, R.raw.uke_f_sharp, 1);
                sounds[11] = pool.load(context, R.raw.uke_g, 1);
                sounds[12] = pool.load(context, R.raw.uke_g_sharp, 1);
                break;
            case VIOLIN:
                sounds = new int[13];
                sounds[0] = pool.load(context, R.raw.violin_a4_low_tenuto_vibrato, 1);
                sounds[1] = pool.load(context, R.raw.violin_a4_tenuto_vibrato, 1);
                sounds[2] = pool.load(context, R.raw.violin_b4_low_tenuto_vibrato, 1);
                sounds[3] = pool.load(context, R.raw.violin_c5_low_tenuto_vibrato, 1);
                sounds[4] = pool.load(context, R.raw.violin_c5_tenuto_vibrato, 1);
                sounds[5] = pool.load(context, R.raw.violin_d5_low_tenuto_vibrato, 1);
                sounds[6] = pool.load(context, R.raw.violin_d5_tenuto_vibrato, 1);
                sounds[7] = pool.load(context, R.raw.violin_e5_tenuto_vibrato, 1);
                sounds[8] = pool.load(context, R.raw.violin_f5_low_tenuto_vibrato, 1);
                sounds[9] = pool.load(context, R.raw.violin_f5_tenuto_vibrato, 1);
                sounds[10] = pool.load(context, R.raw.violin_g4_low_tenuto_vibrato, 1);
                sounds[11] = pool.load(context, R.raw.violin_g4_tenuto_vibrato, 1);
                sounds[12] = pool.load(context, R.raw.violin_g5_tenuto_vibrato, 1);
                break;
            default:
                return null;
        }

        return sounds;
    }
}
