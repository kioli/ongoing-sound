package kioli.sound;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    protected static final String FRAGMENT_TAG = "simple_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            final Fragment fragment = getFragment();
            fragmentTransaction.replace(R.id.base_frame, fragment, FRAGMENT_TAG).commit();
        }
    }

    private Fragment getFragment() {
       int instrument = App.getInstrumentIndex(this);
       Fragment fragment = BaseFragment.newInstance(instrument);
       return fragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Action bar will handle Home/Up button if you specify a parent activity in AndroidManifest.xml.
        Instrument newInstrument;

        switch (item.getItemId()) {
            case R.id.instrument_guitar:
            default:
                newInstrument = Instrument.GUITAR;
                break;
            case R.id.instrument_uke:
                newInstrument = Instrument.UKULELE;
                break;
            case R.id.instrument_violin:
                newInstrument = Instrument.VIOLIN;
                break;
        }

        App.setInstrument(this, newInstrument);
        BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        fragment.changeInstrument(newInstrument);

        return super.onOptionsItemSelected(item);
    }

}
