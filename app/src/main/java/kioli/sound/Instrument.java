package kioli.sound;

public enum Instrument {
    GUITAR(8), VIOLIN(13), UKULELE(8);

    private int notes;

    Instrument(int notes) {
        this.notes = notes;
    }

    public int getNumberNotes() {
        return notes;
    }
}
