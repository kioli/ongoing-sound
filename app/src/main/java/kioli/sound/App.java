package kioli.sound;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class App extends Application {

    private final static String INSTRUMENT_SP_KEY = "instrument_sp";

    private App(){}

    /**
     * Gets a SharedPreferences instance that points to the default file that is
     * used by the preference framework in the given context.
     *
     * @param context The context of the preferences whose values are wanted.
     * @return A SharedPreferences instance that can be used to retrieve and
     * listen to values of the preferences.
     */
    private static SharedPreferences getDefaultSharedPreferences(Context context) {
        return context.getSharedPreferences(getDefaultSharedPreferencesName(context), Context.MODE_PRIVATE);
    }

    private static String getDefaultSharedPreferencesName(Context context) {
        return context.getPackageName() + "_preferences";
    }

    public static int getInstrumentIndex(Context context) {
        SharedPreferences sp = getDefaultSharedPreferences(context);
        return sp.getInt(INSTRUMENT_SP_KEY, 0);
    }

    public static void setInstrument(Context context, Instrument instrument) {
        SharedPreferences sp = getDefaultSharedPreferences(context);
        sp.edit().putInt(INSTRUMENT_SP_KEY, instrument.ordinal()).apply();
    }
}
