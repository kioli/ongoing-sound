package kioli.sound;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import java.util.Random;

public class TouchView extends View {

    private static final int RADIUS = 200;
    private static final long ANIM_DURATION = 200;

    private ShapeDrawable mDrawable;
    private int posX, posY;
    private Random random;

    public TouchView(Context context) {
        super(context);
        random = new Random();
    }

    public void setInfo(final int posX, final int posY) {
        this.posX = posX;
        this.posY = posY;
        setPivotX(posX);
        setPivotY(posY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mDrawable = new ShapeDrawable(new OvalShape());
        int color = Color.argb(58, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        mDrawable.getPaint().setColor(color);
        mDrawable.setBounds(posX - RADIUS / 2, posY - RADIUS / 2, posX + RADIUS / 2, posY + RADIUS / 2);
        mDrawable.draw(canvas);
    }

    public void expandMe(){
        setAlpha(0.0f);
        setScaleX(0);
        setScaleY(0);

        final ObjectAnimator animAlpha = ObjectAnimator.ofFloat(this, View.ALPHA, 1.0f);
        final ObjectAnimator animScaleX = ObjectAnimator.ofFloat(this, View.SCALE_X, 1.0f);
        final ObjectAnimator animScaleY = ObjectAnimator.ofFloat(this, View.SCALE_Y, 1.0f);

        final AnimatorSet set = new AnimatorSet();
        set.playTogether(animAlpha, animScaleX, animScaleY);
        set.setInterpolator(new AccelerateInterpolator());
        set.setDuration(ANIM_DURATION);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                shrinkMe();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                shrinkMe();
            }
        });
        set.start();
    }

    public void shrinkMe(){
        setAlpha(1.0f);
        setScaleX(1);
        setScaleY(1);

        final ObjectAnimator animAlpha = ObjectAnimator.ofFloat(this, View.ALPHA, 0.0f);
        final ObjectAnimator animScaleX = ObjectAnimator.ofFloat(this, View.SCALE_X, 0.0f);
        final ObjectAnimator animScaleY = ObjectAnimator.ofFloat(this, View.SCALE_Y, 0.0f);

        final AnimatorSet set = new AnimatorSet();
        set.playTogether(animAlpha, animScaleX, animScaleY);
        set.setInterpolator(new AccelerateInterpolator());
        set.setDuration(ANIM_DURATION);
        set.start();
    }
}